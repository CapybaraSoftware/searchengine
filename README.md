# SearchEngine

[![CI Status](http://img.shields.io/travis/Pablo Henemann/SearchEngine.svg?style=flat)](https://travis-ci.org/Pablo Henemann/SearchEngine)
[![Version](https://img.shields.io/cocoapods/v/SearchEngine.svg?style=flat)](http://cocoapods.org/pods/SearchEngine)
[![License](https://img.shields.io/cocoapods/l/SearchEngine.svg?style=flat)](http://cocoapods.org/pods/SearchEngine)
[![Platform](https://img.shields.io/cocoapods/p/SearchEngine.svg?style=flat)](http://cocoapods.org/pods/SearchEngine)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SearchEngine is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "SearchEngine"
```

## Author

Pablo Henemann, pablo.henemann@totvs.com.br

## License

SearchEngine is available under the MIT license. See the LICENSE file for more info.
