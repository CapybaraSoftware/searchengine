//
//  ViewController.swift
//  SearchEngine
//
//  Created by Pablo Henemann on 04/26/2016.
//  Copyright (c) 2016 Pablo Henemann. All rights reserved.
//

import UIKit
import SearchEngine

class ViewController: SearchViewController {
	
	@IBOutlet var tableView: UITableView!
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
}

extension ViewController: UITableViewDataSource {
	
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return objects.count
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
		
		let object = objects[indexPath.row] as! NSDate
		cell.textLabel!.text = object.description
		return cell
	}
}

extension ViewController: UITableViewDelegate {
}

extension ViewController: SearchTableViewDataSource {
	
	func getCellForObject(object: AnyObject, listView: UITableView, indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
		
		cell.textLabel!.text = object.description
		return cell
	}
	
	func didSelectItem(listViewCell: UITableViewCell, withObject currentObject: AnyObject) {
	}
}

extension ViewController: SearchEngineDelegate {
	
	func searchOffline(text: String, result: ([AnyObject]) -> Void) {
		let array = self.objects.filter { (date) -> Bool in
			return date.description.containsString(text)
		}
		
		result(array)
	}
	
	func searchOnline(text: String, result: ([AnyObject]) -> Void) {
		
		result([NSDate()])
	}
}