//
//  SearchResultViewController.swift
//  searchEnginePOC
//
//  Created by Pablo Henemann on 4/7/16.
//  Copyright © 2016 Pablo Henemann. All rights reserved.
//

import UIKit

@objc(SearchResultViewController)
class SearchResultViewController: UIViewController {
	
	var searchDelegate: SearchEngineDelegate?
	var searchTableDataSource: SearchTableViewDataSource?
	var searchCollectionDataSource: SearchCollectionDataSource?
	private var isSearching = false
	
	var searching: Bool {
		get {
			return isSearching
		}
		set(newBool) {
			if isSearching != newBool {
				if newBool {
					searchDelegate?.didEnterSearch()
				} else {
					searchDelegate?.didExitSearch()
				}
			}
			isSearching = newBool
		}
	}
	
	@IBOutlet weak var tableView: UITableView!
	
	var sectionsArray = [[String: AnyObject?]]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
}
//MARK: - Tableview
//MARK: Data Source
extension SearchResultViewController: UITableViewDataSource {
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		if let array = self.sectionsArray[section]["array"] as? NSArray {
			return array.count
		} else {
			return 0
		}
	}
	
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return self.sectionsArray.count
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		if let array = self.sectionsArray[indexPath.section]["array"] as? NSArray {
			let obj = array[indexPath.row]
			
			return self.searchTableDataSource?.getCellForObject(obj, listView: tableView, indexPath: indexPath) ?? UITableViewCell()
		}
		
		return UITableViewCell()
	}
}
//MARK: Delegate
extension SearchResultViewController: UITableViewDelegate {
	
	func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		if let title = self.sectionsArray[section]["title"] as? String {
			if title.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).characters.count == 0 {
				return nil
			}
			
			let label = UILabel(frame: CGRect(origin: CGPointZero, size: CGSize(width: tableView.bounds.size.width, height: 30)))
			label.attributedText = NSAttributedString(string: title, attributes: [NSBaselineOffsetAttributeName: 10, NSForegroundColorAttributeName: UIColor.whiteColor(), NSBackgroundColorAttributeName: UIColor.clearColor()])
			
			return label
		}
		return nil
	}
	
	func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		if let title = self.sectionsArray[section]["title"] as? String {
			if title.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).characters.count > 0 {
				return 30
			}
		}
		return 0
	}
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		if let array = self.sectionsArray[indexPath.section]["array"] as? NSArray {
			let obj = array[indexPath.row]
			let cell = tableView.cellForRowAtIndexPath(indexPath)!
			self.searchTableDataSource?.didSelectItem(cell, withObject: obj)
			tableView.reloadData()
		}
	}
}

//MARK: - CollectionView
//MARK: Data Source
extension SearchResultViewController: UICollectionViewDataSource {
	
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		let array = self.sectionsArray[section]["array"] as? NSArray
		return array?.count ?? 0
	}
	
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		if let array = self.sectionsArray[indexPath.section]["array"] as? NSArray {
			let obj = array[indexPath.row]
			return self.searchCollectionDataSource?.getCellForObject(obj, listView: collectionView, indexPath: indexPath) ?? UICollectionViewCell()
		}
		
		return UICollectionViewCell()
	}
	
	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		return self.sectionsArray.count
	}
	
	func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
		if (kind == UICollectionElementKindSectionHeader) {
			if let title = self.sectionsArray[indexPath.section]["title"] as? String {
				if title.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).characters.count > 0 {
					return UICollectionReusableView(frame: CGRect(x: 0, y: 0, width: 5, height: 30))
				}
			}
		}
		
		return UICollectionReusableView(frame: CGRect(x: 0, y: 0, width: 5, height: 30))
	}
}

//MARK: Delegate
extension SearchResultViewController: UICollectionViewDelegate {
	
	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
		
		if let array = self.sectionsArray[indexPath.section]["array"] as? NSArray {
			let obj = array[indexPath.row]
			let cell = collectionView.cellForItemAtIndexPath(indexPath)!
			self.searchCollectionDataSource?.didSelectItem(cell, withObject: obj)
			collectionView.reloadData()
		}
	}
}