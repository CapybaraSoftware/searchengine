//
//  SearchViewController.swift
//  searchEnginePOC
//
//  Created by Pablo Henemann on 4/8/16.
//  Copyright © 2016 Pablo Henemann. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
	
	var searchController: UISearchController!
	var objects = [AnyObject]()
	internal var searchResultViewController: SearchResultViewController!
	
	/*

	 - (void)viewWillDisappear:(BOOL)animated {
	 [super viewWillDisappear:animated];

	 [self.localQueue cancelAllOperations];
	 [self.notificationBarStatus dismissNotification];
	 }

	 - (BOOL)usesCollection {
	 return NO;
	 }

	 - (void)didGetOnlineResultsWithFormattedArray:(NSArray*)arr {
	 [self.searchResultViewController setSectionsArray:arr];
	 }

	 - (NSObject*)getData:(NSInteger)row {

	 if (self.arrItems.count == 0) {
	 return nil;
	 }
	 return [self.arrItems objectAtIndex:row];
	 }

	 - (NSObject*)getFilteredData:(NSIndexPath*)indexPath {
	 return [self.searchResultViewController getFilteredDataInIndexPath:indexPath];
	 }

	 - (UITableView*)getSearchTableView {
	 return [(SearchResultViewController*)self.searchController.searchResultsController getSearchTableView];
	 }

	 - (UICollectionView*)getSearchCollectionView {
	 return [(SearchResultViewController*)self.searchController.searchResultsController getSearchCollectionView];
	 }

	 - (void)reloadSearch {

	 [self.searchResultViewController reloadSearchViews];
	 }

	 */
}

//MARK: UIViewController overrides & funcs
extension SearchViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.searchResultViewController = SearchResultViewController()
		// self.searchResultViewController.delegate = self;
		self.searchController = UISearchController(searchResultsController: self.searchResultViewController)
		self.searchController.searchResultsUpdater = self
		
		// self.searchController.searchBar.placeholder = self.getSearchBarPlaceholder() ?? self.searchController.searchBar.placeholder;
		// Do any additional setup after loading the view.
	}
	func addTwoInts(a: Int, _ b: Int) -> Int {
		return a + b
	}
	override func viewDidDisappear(animated: Bool) {
		super.viewDidDisappear(animated)
		self.searchController.active = false
	}
	
	func configureFilteredArray(array: NSArray) {
		let dict = ["array": array ?? []]
		self.searchResultViewController.sectionsArray = [dict]
		self.searchResultViewController.tableView.reloadData()
	}
	
	func getFilteredArrayForSection(section: NSInteger) -> NSArray? {
		
		return self.searchResultViewController.sectionsArray[section]["array"] as? NSArray
	}
	
	func filterLocalContentForSearchText(searchText: String) {
		
		var stringFilter = String(format: "*%@*", searchText)
		stringFilter = searchText
		
		self.searchResultViewController?.searchDelegate?.searchOffline(stringFilter, result: { [unowned self] filteredArray in
			self.configureFilteredArray(filteredArray)
		})
	}
}

//MARK: UISearchResultsUpdating
extension SearchViewController: UISearchResultsUpdating {
	
	func updateSearchResultsForSearchController(searchController: UISearchController) {
		
		self.searchResultViewController.searching = searchController.active
		
		if let searchString = searchController.searchBar.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()) {
			
			self.filterLocalContentForSearchText(searchString)
			
			if (searchString.characters.count >= 2) {
				
				// TODO: fazer a localQueue para adicionar busca online
				
				// NSBlockOperation* operation = [[NSBlockOperation alloc] init];
				// __weak NSBlockOperation* op = operation;
				//
				// [op addExecutionBlock:^{
				//
				// [NSThread sleepForTimeInterval:1.5];
				//
				// if(![op isCancelled]){
				//
				// dispatch_async( dispatch_get_main_queue(), ^{
				//
				// [self searchOnline:searchString.trim];
				//
				// });
				//
				// }
				//
				// }];
				//
				// [self.localQueue addOperation:op];
			}
		}
	}
}

// MARK: - SearchEngineProtocol

protocol SearchEngineDelegate {
	
	func didEnterSearch()
	func didExitSearch()
	func setSearchBarPlaceholder() -> String?
	
	func searchOnline(text: String, result: ([AnyObject]) -> Void)
	func  searchOffline(text: String, result: ([AnyObject]) -> Void)
}

extension SearchEngineDelegate {
	func didEnterSearch() {
	}
	
	func didExitSearch() {
	}
	
	func searchOnline(text: String) {
	}
	
	func setSearchBarPlaceholder() -> String? {
		return nil
	}
}

enum SearchListType {
	case tableView
	case collectionView
}

protocol SearchTableViewDataSource {
	
	func getCellForObject(object: AnyObject, listView: UITableView, indexPath: NSIndexPath) -> UITableViewCell
	func didSelectItem(listViewCell: UITableViewCell, withObject currentObject: AnyObject)
}

protocol SearchCollectionDataSource {
	
	func getCellForObject(object: AnyObject, listView: UICollectionView, indexPath: NSIndexPath) -> UICollectionViewCell
	func didSelectItem(listViewCell: UICollectionViewCell, withObject currentObject: AnyObject)
	func registerNibForCollectionView(collectionView: UICollectionView)
}

typealias TableProtocol = (SearchTableViewDataSource, SearchEngineDelegate)
typealias CollectionProtocol = (SearchCollectionDataSource, SearchEngineDelegate)
typealias GenericListProtocol = (SearchTableViewDataSource, SearchCollectionDataSource, SearchEngineDelegate)
